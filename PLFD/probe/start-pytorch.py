import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim


def check_torch_cuda():
    print('Cuda-torch info:', '\n')
    print('Is cuda availabe:', torch.cuda.is_available())
    print('Is cuda initialized:', torch.cuda.is_initialized())
    print('Currently used device for cuda:', torch.cuda.current_device(), ', Device name :',
          torch.cuda.get_device_name())
    print('Cuda compute capabilities of device, max, min:', torch.cuda.get_device_capability())
    print('Max cache size from gpu:', torch.backends.cuda.cufft_plan_cache.max_size)
    print('Cuda version: ', torch.version.cuda)


def create_torch_tensor():
    a = torch.ones(5)
    print(a)
    b = a.numpy()
    print(b)


def create_numpy_tensor():
    import numpy as np
    a = np.ones(5)
    b = torch.from_numpy(a)
    c = None
    # add 1 to a tensor and define output
    np.add(a, 1, out=c)
    print(a)
    print(b)
    print(c)


# create_numpy_tensor()


def gradients():
    x = torch.ones(2, 2, requires_grad=True)
    print('\n')
    print('x = ', x)
    y = x + 2
    print('\n')
    print('y = ', y, '\n')
    print('y.grad_fn =', y.grad_fn)

    # same but different

    a = torch.randn(2, 2)
    a = ((a * 3) / (a - 1))
    print('a.requires_grad = ', a.requires_grad)
    a.requires_grad_(True)
    print('a.requires_grad = ', a.requires_grad)
    b = (a * a).sum()
    print('b.grad_fn = ', b.grad_fn, '\n')


# gradients()


class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()

        # 1 input image channel, 5x5 square convolution kernel

        self.conv1 = nn.Conv2d(1, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)

        # an affine (slicna) operation : y = Wx + b, slope of a graph i think

        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        # Max pooling over a (2, 2) window
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))

        # If the size is a square you can only specify a single number
        x = F.max_pool2d(F.relu(self.conv2(x)), 2)

        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))

        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1

        for s in size:
            num_features *= s
            # num_features = num_features * s
        return num_features


def start():
    net = Net()
    print(net)

    # You have to define the ''forward'' function, and the ''backward'' function (where gradients
    # are computed) is automatically defined for you using ''autograd''.
    # You can use any of the Tensor operations in the ''forward'' function
    # The learnable parameters of the model are returned by ''net.parameters()''

    params = list(net.parameters())
    # params.requires_grad()
    print('\n', 'Number of parameters: ', len(params))

    # 6 input channels 1 output channel, 5 x 5 convolution
    print('\n' 'Size of parameters 1: ', params[0].size())  # conv1's .weights
    print('\n' 'Size of parameters 2: ', params[5].size())
    # print('\n', 'Parameters: ', params)

    # Let's try a random 32x32 input
    # Note: Expected input size to this net(LeNet) is 32x32. If planning to use on real images, they
    # should be resized to 32x32

    # N is batch size; D_in is input dimension;
    # H is hidden dimension; D_out is output dimension.
    # (1,1,32,32)

    N, D_in, H, D_out = 1, 1, 32, 32

    input = torch.rand(N, D_in, H, D_out)

    out = net(input)
    print('\n', 'This is output tensor: ', out)

    # Zero the gradient buffers of all parameters and backprops with random gradients:

    net.zero_grad()
    out.backward(torch.randn(1, 10))

    # NOTES
    '''
        torch.nn only supports mini-batches. The entire 'torch.nn' package only supports inputs 
        that are a mini-batch of samples, and not a single sample.
        
        For example, ''nn.Conv2d'' will taka in a 4D Tensor of ''nSamples x nChannels x Height x Width''.
        
        If you have a single sample, just use ''input.unsqueeze (0) to add a fake batch dimension.
    '''

    # Loss function
    ''''
    -------------------
         A loss function takes the (output, target) pair of inputs, and computes a value that estimates 
         how far away the output is from the target.
         
         There are several different  'loss' functions under the nn package.
         A simple loss is: ''nn.MSELoss'' which computes the mean-squared error
         between the input and the target.
        
        For example:
    --------------------
    '''

    output = net(input)
    target = torch.randn(10)  # a dummy target, for example
    target = target.view(1, -1)  # make it the same shape as output

    criterion = nn.MSELoss()

    loss = criterion(output, target)
    print('\n', 'This is output value from the loss function: ', loss, '\n')

    def print_graph(g, level=0):
        if g == None: return
        print('*' * level * 4, g)
        for subg in g.next_functions:
            print_graph(subg[0], level + 1)

    print_graph(loss.grad_fn, 0)

    '''
        Now if you follow ''loss'' in the backward direction, using it's ''.grad_fn'' attribute,
        you will see a graph of computations. So when we call ''loss.backward()'', the whole graph 
        is differentiated with respect to the loss, and all Tensors in the graph that has
        ''requires_grad=True'' will have their ''.grad'' Tensor accumulated with the gradient.
        
        For illustration, let us follow a few steps backwards:
    '''

    print('MSELoss: ', loss.grad_fn)  # MSELoss
    print('Linear: ', loss.grad_fn.next_functions[0][0])  # Linear
    print('ReLu: ', loss.grad_fn.next_functions[0][0].next_functions[0][0])  # ReLu


    # Backprop

    '''
        To backpropagate the error all we have to do is ''loss.backward()''.
        You need to clear the existing gradients though, else gradients will be 
        accumulated to existing gradients.
        
        Now we shall call ''loss.backward()'', and have a look at conv1's bias gradients
        before and after backward.
        
    '''

    net.zero_grad()  # zeroes the gradient buffers of all parameters

    print('\n', 'conv1.bias.grad before backward')
    print(net.conv1.bias.grad)

    loss.backward(create_graph=True)
    # loss.grad_fn()

    print('\n', 'conv1.bias.grad after backward')
    print(net.conv1.bias.grad)

    weights_update(net, criterion, target, input)


def weights_update(net, criterion, target, input):
    # The simplest update rule used in practice is the Stochastic Gradient Descent (SGD)
    # ''weight = weight - learning_rate * gradient''

    learning_rate = 0.01
    net_parameters = net.parameters()
    optimizer = optim.SGD(net.parameters(), lr=0.01)

    for f in net_parameters:
        f.data.sub_(f.grad.data * learning_rate)
        # print(f)

        # Various different update rules are: SGD,  Nesterov-SGD, Adam, RMSProp, etc.
        # To enable this a small package was built ''torch.optim'' that implements all these methods

        # Create your optimizer

        # In your training loop

        optimizer.zero_grad()  # Zero the gradient buffers
        output = net(input)
        loss = criterion(output, target)

        print('\n', 'This is output value from the 2-loss function: ', loss, '\n')

        loss.backward()

        # print('\n', 'This is output value from the loss.backward function: ', back, '\n')

        optimizer.step()  # Does the update

        print(optimizer)







'''
    for input, target in dataset:
    optimizer.zero_grad()
    output = model(input)
    loss = loss_fn(output, target)
    loss.backward()
    optimizer.step()
 '''

#
'''
    change type

x = torch.rand_like(x, dtype = torch.float)

    resizing

x = torch.randn(4,4)
y = x.view(16)
z = x.view(-1, 8) # the size -1 is inferred from other dimensions 

    access number in tensor 
    
x.item()

print (x.size(), y.size(), z.size())


    move tensor to cuda
    
if torch.cuda.is_available():

    device = torch.device('cuda')   # cuda object
    y = torch.ones_like(x, device = device)     # directly create a tensor on GPU
    x = x.to(device)    # or just use string ''.to('cuda')''
    z = x + y
    
    
    Inputs -> Calculate loss -> Back propagation ->  Update parameters
    
    output_batch = model (train_batch)      # compute model output
    
    loss = loss_fn (output_batch, labels_batch)     # calculate loss
    
    optimizer.zero_grad()   # clear previous gradients
    loss.backward ()    # compute gradients of all variables with loss
    
    optimizer.step()    # perform updates using calculated gradients
    
     
    
    
'''