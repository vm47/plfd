import tensorflow as tf
import os
import keras

keras.__version__



def test_tf():
    tf.debugging.set_log_device_placement(True)

    # Create some tensors
    a = tf.constant([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])
    b = tf.constant([[1.0, 2.0], [3.0, 4.0], [5.0, 6.0]])
    print(tf.matmul(a, b), '/n')

    print('Tensorflow version is :', tf.__version__)
    print('Tensorflow gpu device name :', tf.test.gpu_device_name())
    print('Is Tensorflow built with cuda :', tf.test.is_built_with_cuda())
    print('Is Tensorflow built with gpu support :', tf.test.is_built_with_gpu_support())


test_tf()
# torch.cuda.is_available()