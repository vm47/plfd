'''
###########################################################################

This is a piece of code for submitting to competition - I'm planning to improve this code further
For now, for proof of concept for master thesis this should be enough


---------------------------------------------------------------------------
Larger network:

class Net(nn.Module):

    def __init__(self, device):
        super(Net, self).__init__()
        self.lstm_1 = nn.LSTM(57, 128, 1, batch_first=True, bidirectional=True)
        self.lstm_2 = nn.LSTM(input_size=57, hidden_size=64, num_layers=1, bias=True, batch_first=True,
                            bidirectional=True)

        # self.attention = Attention(input_shape[1])(self.lstm) # this is a way of impl it in keras
        self.linear_1 = nn.Linear(in_features=40, out_features=32)  # matmul operation
        self.linear_2 = nn.Linear(in_features=32, out_features=1)  # matmul operation

    def forward(self, input_tensor):
        # self.lstm_1.flatten_parameters()
        # implement the forward pass
        lstm = self.lstm_1(input_tensor)
        # lstm = self.lstm_2(lstm)

        lstm = self.linear_1(lstm)
        lstm = F.relu(lstm)

        lstm = self.linear_2(lstm)
        lstm = F.sigmoid(lstm)

-----------------------------------------------------------------------------------

For submission:



total_loss = []
total_correct = []
dataset_test = TensorDataset(test, validate)
dataloader_test = DataLoader(dataset_test, batch_size=64, shuffle=True)


for test_data, labels in dataloader_test:  # get batch

    predictions = net(test_data.float())  # Pass batch, predictions contains weights of the network and output tensors
    # preds.append(predictions)
    # print(labels.dtype, predictions.shape)
    loss = F.binary_cross_entropy_with_logits(predictions[:, 0], labels.float())  # Calculate loss
    # print(loss, loss.dtype, loss.shape)

    total_loss.append(loss.item())
    total_correct.append(get_num_correct(predictions, labels))

    results = matthews_correlation(labels, predictions)

print('total_correct: ', total_correct,
      'loss: ', total_loss)


# The output of this kernel must be binary (0 or 1), but the output of the NN Model is float (0 to 1).
# So, find the best threshold to convert float to binary is crucial to the result
# this piece of code is a function that evaluates all the possible thresholds from 0 to 1 by 0.01
def threshold_search(y_true, y_proba):
    best_threshold = 0
    best_score = 0
    for threshold in tqdm([i * 0.01 for i in range(100)]):
        # score = K.eval(matthews_correlation(y_true.astype(np.float64), (y_proba > threshold).astype(np.float64)))
        score = (matthews_correlation(y_true.astype(np.float64), (y_proba > threshold).astype(np.float64)))

        if score > best_score:
            best_threshold = threshold
            best_score = score
    search_result = {'threshold': best_threshold, 'matthews_correlation': best_score}
    return search_result


best_threshold = threshold_search(validate, preds)['threshold']

submission = pd.read_csv('/vsb-power-line-fault-detection/sample_submission.csv')
print(len(submission))
submission.head()

preds_test = []
for i in range(epochs):
    # net.load_weights('weights_{}.h5'.format(i))
    # pred = model.predict(X_test_input, batch_size=300, verbose=1)
    pred_3 = []
    for pred_scalar in predictions:
        for i in range(3):
            pred_3.append(pred_scalar)
    preds_test.append(pred_3)

preds_test = (np.squeeze(np.mean(preds_test, axis=0)) > best_threshold).astype(np.int)
preds_test.shape

submission['target'] = preds_test
submission.to_csv('submission.csv', index=False)
submission.head()



###########################################################################

This are the examples from web


class Net(nn.Module):

    def __init__(self, input_size, hidden_size, output_size, device):
        super(Net, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.lstm = nn.LSTMCell(self.input_size, self.hidden_size)
        self.linear = nn.Linear(self.hidden_size, self.output_size)

    def forward(self, input, future=0, y=None):
        outputs = []

        # reset the state of LSTM
        # the state is kept till the end of the sequence
        h_t = torch.zeros(input.size(1), self.hidden_size, dtype=torch.float64)
        c_t = torch.zeros(input.size(1), self.hidden_size, dtype=torch.float64)

        for i, input_t in enumerate(input.chunk(input.size(1), dim=1)):
            h_t, c_t = self.lstm(input_t.float(), (h_t, c_t))
            output = self.linear(h_t)
            outputs += [output]

        for i in range(future):
            if y is not None and random.random() > 0.5:
                output = y[:, [i]]  # teacher forcing
            h_t, c_t = self.lstm(output, (h_t, c_t))
            output = self.linear(h_t)
            outputs += [output]
        outputs = torch.stack(outputs, 1).squeeze(2)
        return outputs



#########################################################

# this should be a way to input train and validate values

for i, data in enumerate(trainloader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()


#########################################################

# Attention source code from pytorch, I haven't implemented the Attention class, which is also a big deal


class Attention(nn.Module):
    """ Applies attention mechanism on the `context` using the `query`.

    **Thank you** to IBM for their initial implementation of :class:`Attention`. Here is
    their `License
    <https://github.com/IBM/pytorch-seq2seq/blob/master/LICENSE>`__.

    Args:
        dimensions (int): Dimensionality of the query and context.
        attention_type (str, optional): How to compute the attention score:

            * dot: :math:`score(H_j,q) = H_j^T q`
            * general: :math:`score(H_j, q) = H_j^T W_a q`

    Example:

         >>> attention = Attention(256)
         >>> query = torch.randn(5, 1, 256)
         >>> context = torch.randn(5, 5, 256)
         >>> output, weights = attention(query, context)
         >>> output.size()
         torch.Size([5, 1, 256])
         >>> weights.size()
         torch.Size([5, 1, 5])
    """

    def __init__(self, dimensions, attention_type='general'):
        super(Attention, self).__init__()

        if attention_type not in ['dot', 'general']:
            raise ValueError('Invalid attention type selected.')

        self.attention_type = attention_type
        if self.attention_type == 'general':
            self.linear_in = nn.Linear(dimensions, dimensions, bias=False)

        self.linear_out = nn.Linear(dimensions * 2, dimensions, bias=False)
        self.softmax = nn.Softmax(dim=-1)
        self.tanh = nn.Tanh()

[docs]
    def forward(self, query, context):
        """
        Args:
            query (:class:`torch.FloatTensor` [batch size, output length, dimensions]): Sequence of
                queries to query the context.
            context (:class:`torch.FloatTensor` [batch size, query length, dimensions]): Data
                overwhich to apply the attention mechanism.

        Returns:
            :class:`tuple` with `output` and `weights`:
            * **output** (:class:`torch.LongTensor` [batch size, output length, dimensions]):
              Tensor containing the attended features.
            * **weights** (:class:`torch.FloatTensor` [batch size, output length, query length]):
              Tensor containing attention weights.
        """
        batch_size, output_len, dimensions = query.size()
        query_len = context.size(1)

        if self.attention_type == "general":
            query = query.reshape(batch_size * output_len, dimensions)
            query = self.linear_in(query)
            query = query.reshape(batch_size, output_len, dimensions)

        # TODO: Include mask on PADDING_INDEX?

        # (batch_size, output_len, dimensions) * (batch_size, query_len, dimensions) ->
        # (batch_size, output_len, query_len)
        attention_scores = torch.bmm(query, context.transpose(1, 2).contiguous())

        # Compute weights across every context sequence
        attention_scores = attention_scores.view(batch_size * output_len, query_len)
        attention_weights = self.softmax(attention_scores)
        attention_weights = attention_weights.view(batch_size, output_len, query_len)

        # (batch_size, output_len, query_len) * (batch_size, query_len, dimensions) ->
        # (batch_size, output_len, dimensions)
        mix = torch.bmm(attention_weights, context)

        # concat -> (batch_size * output_len, 2*dimensions)
        combined = torch.cat((mix, query), dim=2)
        combined = combined.view(batch_size * output_len, 2 * dimensions)

        # Apply linear_out on every 2nd dimension of concat
        # output -> (batch_size, output_len, dimensions)
        output = self.linear_out(combined).view(batch_size, output_len, dimensions)
        output = self.tanh(output)

        return output, attention_weights


#####################################################################################

'''
