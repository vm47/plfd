import numpy as np
import torch

torch_file = "Outputs/torch_data.pt"
device = torch.device("cuda")

data1 = np.load('Outputs/X.npy')
data2 = np.load('Outputs/y.npy')


print(data1.shape,)# data2.shape)
# print(data2)


loaded = torch.load(torch_file)
tensor_a = loaded['torch_train']
tensor_b = loaded['torch_val']
tensor_c = loaded['test_tensor']

# train = torch.squeeze(tensor_a, 0)
# validate = tensor_b.transpose(1, 0)

print(tensor_a.shape,) #tensor_b.shape, tensor_c.shape)
print(tensor_b.shape)
print(tensor_c.shape)

# third_tensor = torch.cat((tensor_a[0][0], tensor_a[0][1]), 0)

# print(third_tensor.shape)

data1 = torch.as_tensor(data1)
data2 = torch.as_tensor(data2)

# print(torch.all(data1.eq(tensor_a)))

print(torch.all(tensor_a.eq(data1)))
print(torch.all(tensor_b.eq(data2)))