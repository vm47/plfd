import torch
import pandas as pd
import pyarrow.parquet as pq  # Used to read the data
import numpy as np
from tqdm import tqdm


## In this notebook i'm just extracting features in a way that showed the best from this source :
## https://www.kaggle.com/braquino/5-fold-lstm-attention-fully-commented-0-694

# in jupyter notebook I have extracted the min and max values from the train data, the measurements

TEST_DATA = []
# TRAIN_DATA = []
# VALIDATE_DATA = []
max_num = 127
min_num = -128
sample_size = 800000
metadata_test_csv = 'vsb-power-line-fault-detection/metadata_test.csv'
test_measurements = 'vsb-power-line-fault-detection/test.parquet'
metadata_train = 'vsb-power-line-fault-detection/metadata_train.csv'
train_measurements = 'vsb-power-line-fault-detection/train.parquet'
torch_file = "Outputs/torch_data_3.pt"


def load_meta_train_data(meta_train):
    df_train = pd.read_csv(meta_train)
    df_train = df_train.set_index(['id_measurement', 'phase'])

    return df_train


def load_test_data(meta_test):
    metadata_test = pd.read_csv(meta_test)
    metadata_test = metadata_test.set_index(['signal_id'])
    # print(metadata_test.head())
    # print(metadata_test.tail())

    first_sig = metadata_test.index[0]

    n_parts = 10
    max_line = len(metadata_test)
    part_size = int(max_line / n_parts)
    last_part = max_line % n_parts
    print('\n', 'first_sig: ', first_sig, 'n_parts: ', 'n_parts: ', n_parts,
          'max_line: ', max_line, 'part_size: ', part_size, 'last_part: ', last_part,
          'len: ', n_parts * part_size + last_part, '\n')

    # Create n equal parts in range of metadata_test values, and store them in list; list starts from 8712 so we need
    # to enlarge for loop range by that number

    start_end = [[x, x + part_size] for x in range(first_sig, max_line + first_sig, part_size)]

    # In previous list last member was not correct (29042, 31075), so we need to exclude that element of list and create
    # new correct one, that is happening in this line, list of list's from first element of list of list's 'start_end'
    # to the first element of last member of start_end l of l plus last_part extracted before as module

    start_end = start_end[:-1] + [[start_end[-1][0], start_end[-1][0] + last_part]]

    # print('individual components: ', start_end[:1], start_end[-1][0], start_end[-1][0], last_part,
    #       (start_end[-1][0] + last_part),
    #       start_end[-1][1])

    print('start_end =', start_end)

    return start_end, metadata_test


# After making peaces of meta test data, we are doing the same with measurements, but combining those
# two sets of data in one (measurements with metadata ie. label's - targets)


def load_train_measurements(train_data, start, end):
    # total_size = len(load_train_data())
    # print(total_size)
    # start = 0
    # end = 8712

    measurements = pq.read_pandas(train_data, columns=[str(i) for i in range(start, end)]).to_pandas()

    # print(measurements)

    return measurements


# This function standardizes max and min values from measurements to (-1, 1), in an attempt to help NN
# You should just try this with and without converting it, collected tensor will be in range -128 to 127
# instead of -1, 1

def transfer_min_max(ts, min_data, max_data, range_needed=(-1, 1)):
    if min_data < 0:
        ts_std = (ts + abs(min_data)) / (max_data + abs(min_data))
        # print('min_Data = ', ts_std)
    else:
        ts_std = (ts - min_data) / (max_data - min_data)
        # print('else min data : ', ts_std)

    if range_needed[0] < 0:
        return ts_std * (range_needed[1] + abs(range_needed[0])) + range_needed[0]
        # print('range_needed < 0 : ', ts_std * (range_needed[1] + abs(range_needed[0])) + range_needed[0])
    else:
        return ts_std * (range_needed[1] - range_needed[0]) + range_needed(0)
        # print('else_range : ', ts_std * (range_needed[1] - range_needed[0]) + range_needed(0))


# Measurement were taken for every one of 3 phases, 800 000 times, resulting in 2,4 milions of data, best approach
# i read so far was to divide measurements of each phase in n_dimensions (parts) containing n features. Each part
# contains 5000 measurements (800000 / 160), and the features are extracted from this 5000 measurements piece

def transform_data_ts(ts, n_dim=160, min_max=(-1, 1)):
    # here we convert data to -1,1 for every chunk in single measurement
    time_stamp_range = transfer_min_max(ts, min_data=min_num, max_data=max_num)

    # chunk size; 5000 = 800000 / 160
    chunk_size = int(sample_size / n_dim)

    # container of new data (160 dimensions of every measurement with their 19 features)
    new_chunk_ts = []

    for i in range(0, sample_size, chunk_size):
        # get every value in chunk in transformed_ts range
        ts_range = time_stamp_range[i:i + chunk_size]

        # Calculate Features

        mean = ts_range.mean()
        # mean = torch.mean(ts_range)  # mean value of chunk

        std = ts_range.std()
        # std = torch.std(ts_range)  # standard deviation

        std_top = mean + std
        std_bot = mean - std

        # Percentages
        percent_calc = np.percentile(ts_range, [0, 1, 25, 50, 75, 99, 100])

        # Amplitude of chunk
        max_range = percent_calc[-1] - percent_calc[0]

        # Relative percentile
        relative_percentile = percent_calc - mean  # added to features as hope for help

        # Add features to new_chunk_ts, and convert it to to np.array and torch.tensor
        new_chunk_ts.append(np.concatenate([np.asarray([mean, std, std_top, std_bot, max_range]),
                                            percent_calc, relative_percentile]))

    return np.asarray(new_chunk_ts)


# Concatenate metadata test values with test measurements and append that along with features
# in torch tensor


def convert_test_data(meta):
    start_end, metadata_test = load_test_data(meta)

    # if you want to check functionality, just uncomment start and end to not wait the whole
    # data been procesed

    # start = 8712
    # end = 10745
    for start, end in start_end:
        subset_test = pq.read_pandas(test_measurements,
                                     columns=[str(i) for i in range(start, end)]).to_pandas()

        # if you find columns with broken data in it, this debugging should help you

        # subset_test.columns = subset_test.columns.str.strip('')
        # print(subset_test.columns)
        # broken_col = "{}".format(subset_test.columns[1])
        # print(subset_test.loc(int(broken_col)))
        # a = subset_test.colums.tolist()
        # print(subset_test.columns.values)

        for signal_id in tqdm(subset_test.columns.values):
            id_measurement, phase = metadata_test.loc[int(signal_id)]
            subset_test_col = subset_test[signal_id]
            # subset_test_col = subset_test.loc[int(signal_id)]
            subset_transformed_pt = transform_data_ts(subset_test_col)
            TEST_DATA.append([signal_id, id_measurement, phase, subset_transformed_pt])


def create_test_data():
    convert_test_data(metadata_test_csv)

    # create numpy tensor
    input_test_data = np.asarray([np.concatenate([TEST_DATA[i][3], TEST_DATA[i + 1][3], TEST_DATA[i + 2][3]], axis=1)
                                  for i in range(0, len(TEST_DATA), 3)])

    # create torch tensor
    # torch_test_input = torch.as_tensor(input_test_data)

    return input_test_data


def prep_train_data(start, end):
    measurements = load_train_measurements(train_measurements, start, end)
    metadata = load_meta_train_data(metadata_train)

    train = []
    validate = []

    # For every 3 measurement of each phase extract the data, and process it ie. extract features from it,
    # append them to list and create a numpy array's, append target data only once - from the first phase

    for id_measurement in tqdm(metadata.index.levels[0].unique()[int(start / 3):int(end / 3)]):
        train_signal = []
        for phase in [0, 1, 2]:
            signal_id, target = metadata.loc[id_measurement].loc[phase]

            # Even this seems to be wrong, there is no too much error in this workaround,
            # because result showed no differnce in having a labeled all three phases versus taking the target
            # from the first phase, competition any way asked to label all three phases as one, which means if
            # there's a fault in one phase, label them all as faulty, and this way showed better results

            if phase == 0:
                validate.append(target)

            # append features from selected chunk data
            train_signal.append(transform_data_ts(measurements[str(signal_id)]))

        # concatenate all 3 phases to one matrix
        train_signal = np.concatenate(train_signal, axis=1)

        # add data to list of train features
        train.append(train_signal)

    # convert data to numpy array's
    train = np.asarray(train)
    validate = np.asarray(validate)

    return train, validate


def create_train_data():
    Train = []
    Validate = []

    total_size = len(load_meta_train_data(metadata_train))

    # divide data to 2 equal part's and proces it, to not cause memory errors

    for start, end in [(0, int(total_size / 2)), (int(total_size / 2), total_size)]:
        train, validate = prep_train_data(start, end)

        # save data from two parts
        Train.append(train)
        Validate.append(validate)

    # store them to global variables, this creates tensor in shape of (2, 1452, 160, 57) for train
    # and (1, 2904) for validate data ie. target

    # TRAIN_DATA.append(Train)
    # VALIDATE_DATA.append(Validate)

    # this creates tensor in shape (1, 2904, 160, 57) for train and (1, 2904) for validate data ie. target

    TRAIN_DATA = np.concatenate(Train)
    VALIDATE_DATA = np.concatenate(Validate)

    return TRAIN_DATA, VALIDATE_DATA


def save_test_train():
    # create_train_data()

    # to try

    # TRAIN_DATA = np.concatenate(TRAIN_DATA)
    # VALIDATE_DATA = np.concatenate(VALIDATE_DATA)

    # train_tensor, val_tensor = np.concatenate(TRAIN_DATA), np.concatenate(VALIDATE_DATA)

    train_tensor, val_tensor = create_train_data()
    torch_train = torch.as_tensor(train_tensor)
    torch_val = torch.as_tensor(val_tensor)
    test_tensor = torch.as_tensor(create_test_data())

    data = {'torch_train': torch_train, 'torch_val': torch_val, 'test_tensor': test_tensor}

    torch.save(data, torch_file)


save_test_train()


'''
Selecting data by row numbers (.iloc)
Selecting data by label or by a conditional statment (.loc)
'''