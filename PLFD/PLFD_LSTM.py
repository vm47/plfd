import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from tqdm import tqdm
from torch.utils.data import TensorDataset, DataLoader

torch_file = "Outputs/torch_data.pt"
device = torch.device("cuda")


def check_torch_cuda():
    print('Cuda-torch info:', '\n')
    print('Is cuda availabe:', torch.cuda.is_available())
    print('Is cuda initialized:', torch.cuda.is_initialized())
    print('Currently used device for cuda:', torch.cuda.current_device(), ', Device name :',
          torch.cuda.get_device_name())
    print('Cuda compute capabilities of device, max, min:', torch.cuda.get_device_capability())
    print('Max cache size from gpu:', torch.backends.cuda.cufft_plan_cache.max_size)
    print('Cuda version: ', torch.version.cuda)


# This is where we round the prediction values to highest integer and compare them to labels
def get_num_correct(pred, lab):

    predicted = pred.argmax(dim=1).eq(lab).sum().item()
    num = torch.numel(lab)
    result = predicted / num
    return predicted, result


def load_tensors():
    loaded = torch.load(torch_file, device)
    tensor_a = loaded['torch_train']
    tensor_b = loaded['torch_val']
    tensor_c = loaded['test_tensor']

    # train = torch.squeeze(tensor_a, 0)
    # validate = tensor_b.transpose(1, 0)

    return tensor_a, tensor_b, tensor_c


class Net(nn.Module):
    def __init__(self, input_size=57, hidden_size=128, num_layers=2, output_size=1):
        super(Net, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers, batch_first=True)
        self.fc = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        out, _ = self.lstm(x)
        out = self.fc(out[:, -1, :])
        return out


# bad nvidia driver at work : RuntimeError: round_vml_cpu not implemented for 'Long
net = Net().to(device)
check_torch_cuda()

# send tensors to cpu only
# net = Net()

train, validate, test = load_tensors()
optimizer = optim.Adam(net.parameters(), lr=0.01)

epochs = 100
batch_size = 1500

dataset = TensorDataset(train, validate)
dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)


for epoch in tqdm(range(epochs)):
    total_loss = []
    total_correct = []
    results = []

    for train_data, labels in dataloader:  # get batch

        predictions = net(train_data.float())  # Pass batch, predictions
        # contains weights of the network and output tensors
        # preds.append(predictions)
        loss = F.binary_cross_entropy_with_logits(predictions[:, 0], labels.float())  # Calculate loss

        optimizer.zero_grad()
        loss.backward()  # Calculate gradients
        optimizer.step()  # Update weights

        l = round(loss.item(), 3)

        total_loss.append(l)
        pred, res = get_num_correct(predictions, labels)
        total_correct.append(pred)
        results.append(res)
        # results.append()

    print('\n', 'Epoch: ', epoch, '\n',
          'total_correct: ', total_correct,
          'loss: ', total_loss, '\n',
          'results :', results)
